import { Typography } from "antd";
import "./Footer.scss";

const Footer = () => {
  return (
    <footer className="footer">
      <Typography.Text>2023 © TRY.</Typography.Text>
      <Typography.Text>Powered by DuongLee.</Typography.Text>
    </footer>
  );
};

export default Footer;
