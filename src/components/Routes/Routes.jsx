import { Route, Routes } from "react-router-dom";
import Account from "../../pages/Account/Account";
import Dashboard from "../../pages/Dashboard/Dashboard";

const AllRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<Dashboard />}></Route>
      <Route path="/account" element={<Account />}></Route>
    </Routes>
  );
};

export default AllRoutes;
