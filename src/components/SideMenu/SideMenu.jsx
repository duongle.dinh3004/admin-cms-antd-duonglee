import {
  AppstoreAddOutlined,
  BookOutlined,
  CalendarOutlined,
  FormOutlined,
  SettingOutlined,
  UsergroupDeleteOutlined,
} from "@ant-design/icons";
import { Menu } from "antd";
import { useNavigate } from "react-router-dom";

const SideMenu = () => {
  const navigate = useNavigate();
  return (
    <nav className="SideMenu">
      <Menu
        onClick={(item) => {
          navigate(item.key);
        }}
        items={[
          {
            label: "Dashboard",
            icon: <AppstoreAddOutlined />,
            key: "/",
          },
          {
            label: "Account Settings",
            icon: <SettingOutlined />,
            key: "/account",
          },
          {
            label: "Forms",
            icon: <FormOutlined />,
            key: "/forms",
          },
          {
            label: "Calendar",
            icon: <CalendarOutlined />,
            key: "/calendar",
          },
          {
            label: "Products",
            icon: <BookOutlined />,
            key: "/products",
          },
          {
            label: "Support",
            icon: <UsergroupDeleteOutlined />,
            key: "/support",
          },
        ]}
      ></Menu>
    </nav>
  );
};

export default SideMenu;
