import { Badge, Image, Space } from "antd";
import "./Header.scss";
import logo from "../../assets/try.jpg";
import { BarsOutlined, BellOutlined, MessageOutlined } from "@ant-design/icons";
import Search from "antd/es/input/Search";

const Header = () => {
  return (
    <header className="header">
      <Space>
        <BarsOutlined className="icon" />
        <Search placeholder="Search" enterButton />
      </Space>

      <Space size="large">
        <Badge count={12}>
          <BellOutlined className="icon" />
        </Badge>
        <Badge count={21}>
          <MessageOutlined className="icon" />
        </Badge>
        <Image
          className="logo"
          width={40}
          height={40}
          src={logo}
          alt="logo"
        ></Image>
      </Space>
    </header>
  );
};

export default Header;
