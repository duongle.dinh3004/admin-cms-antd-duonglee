import { Space } from "antd";
import "./App.scss";
import Header from "./components/Header/Header";
import Content from "./components/Content/Content";
import SideMenu from "./components/SideMenu/SideMenu";
import Footer from "./components/Footer/Footer";

function App() {
  return (
    <div className="App">
      <Header />
      <Space className="sidemenu-content">
        <SideMenu />
        <Content />
      </Space>
      <Footer />
    </div>
  );
}

export default App;
